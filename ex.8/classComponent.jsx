import React, { Component } from 'react';

// define a component as a class
export default class ClassComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { value: props.initialValue }
    }

    sum(delta) {
        this.setState({ value: this.state.value + delta });
    }

    // same function but change state for more atributes
    sumMoreAtributes(delta){
        this.setState({ ...this.state, value: this.state.value + delta });
    }

    render() {
        return (
            <div>
                <h1>{this.props.label}</h1>
                <h1>{this.state.value}</h1>
                <button onClick={() => this.sum(-1)}>Decrease</button>
                <button onClick={() => this.sum(1)}>Increase</button>
            </div>
        )
    }
}