import React from 'react';
import { childrenWithProps } from '../utils/reactUtils';

// receive props from children

export default props => (
    <div>
        <h1>Family</h1>
        { childrenWithProps(props.children, props) }
    </div>
);
