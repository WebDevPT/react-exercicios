import React, { Component } from 'react';

// define a component as a class
export default class ClassComponent extends Component {
    render() {
        return (
            <h1>{this.props.value}</h1>
        )
    }
}