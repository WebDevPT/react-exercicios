import React from 'react';
import ReactDOM from 'react-dom';

import Family from './family';
import Member from './member';

// call main component
// pass props from children to main component

ReactDOM.render(
    <Family  lastName='Doe'>
        <Member name='John' />
        <Member name='Mary' />
        <Member name='Steve' />
        <Member name='Maria' />
    </Family>
    ,document.getElementById('app')
);