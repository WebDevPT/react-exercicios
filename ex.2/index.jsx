import React from 'react';
import ReactDOM from 'react-dom';
// import component
import Component from './component'

ReactDOM.render(
    <Component />,
    document.getElementById('app')
);