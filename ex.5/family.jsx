import React from 'react';
import Member from './member';

export default props => (
    <div>
        <Member name='John' lastName='Doe' />
        <Member name='Rita' lastName='Doe' />
        <Member name='Mary' lastName='Doe' />
        <Member name='Steve' lastName='Doe' />
    </div>
);
