import React from 'react';

// component created using a function
export default () => (
    <h1>First Component!</h1>
);

// component created using a class
