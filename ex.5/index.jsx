import React from 'react';
import ReactDOM from 'react-dom';
// import main component
import Family from './family';

ReactDOM.render(
    // Main Component that contains children component inside (members)
    <Family />
    ,document.getElementById('app')
);