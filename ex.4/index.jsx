import React from 'react';
import ReactDOM from 'react-dom';
// import both components from component file
import FirstComponent, { SecondComponent } from './component'

ReactDOM.render(
    <div>
        <FirstComponent />
        <SecondComponent />    
    </div>
    ,document.getElementById('app')
);