import React from 'react';

// create component 1 and export default
export default props => (
    <h1>First component!</h1>
);

// create component 2

const SecondComponent = props => (
    <h1>Second component!</h1>
);

// export SecondComponent
export { SecondComponent };
