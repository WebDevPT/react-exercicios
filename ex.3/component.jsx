import React from 'react';

// component created using a function
export default (props) => (
     // receive props from index.html component instance
     // show value atribute from props
    <h1>{props.value}</h1>
);
